import generation.ActionLanguageGenerator;
import generation.AssociationClassGenerator;
import generation.AttributeGenerator;
import generation.ConstructorGenerator;
import generation.ImplementsGenerator;
import generation.ImportGenerator;
import generation.MethodGenerator;
import generation.StateGenerator;
import generation.XmlParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import model.ClassDiagram;

public class test {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		// XmlParser xmlparser = new XmlParser("microwaveCorrectAS.xml");
		XmlParser xmlparser = new XmlParser("casestudyBookStore.xml");

		
		ClassDiagram classDiagram = xmlparser.parse();

		ImplementsGenerator implGen = new ImplementsGenerator();
		AssociationClassGenerator assGen = new AssociationClassGenerator();
		AttributeGenerator attrGen = new AttributeGenerator();
		ConstructorGenerator consGen = new ConstructorGenerator();
		ActionLanguageGenerator alGen = new ActionLanguageGenerator();
		MethodGenerator methGen = new MethodGenerator();
		StateGenerator stateGen = new StateGenerator();
		ImportGenerator importGen = new ImportGenerator();

		
		assGen.setNext(attrGen);
		attrGen.setNext(consGen);
		consGen.setNext(alGen);
		alGen.setNext(methGen);
		methGen.setNext(stateGen);
		stateGen.setNext(importGen);
		importGen.setNext(implGen);
		
		assGen.handle(classDiagram);

		for (model.Class clas : classDiagram.getClasses().values()) {
			System.out.println("output/" + clas.getName() + ".java");
			File file = new File("output/" + clas.getName() + ".java");
			PrintWriter writer = new PrintWriter(file, "UTF-8");
			writer.println(clas);
			writer.close();
		}

		System.out.println("klaar");
	}
}
