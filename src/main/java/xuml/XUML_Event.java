package xuml;

public class XUML_Event {
	private String name;
	private XUML_State from;
	private XUML_State to;
	private int uniqueId = -1;

	public XUML_Event(String name, XUML_State from, XUML_State to) {
		super();
		this.name = name;
		this.from = from;
		this.to = to;
	}

	public XUML_Event(String name, XUML_State from, XUML_State to, int uniqueId) {
		this(name, from, to);
		this.uniqueId = uniqueId;
	}

	public String getName() {
		return name;
	}

	public XUML_State getFrom() {
		return from;
	}

	public XUML_State getTo() {
		return to;
	}

	public int getUniqueId() {
		return this.uniqueId;
	}

	public void setuniqueId(int id) {
		this.uniqueId = id;
	}

}
