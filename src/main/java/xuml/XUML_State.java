package xuml;

import java.util.Map;

public interface XUML_State {
	public void process(Map<String,Object> parameters);
}
