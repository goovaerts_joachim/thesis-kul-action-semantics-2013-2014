package xuml;

import java.util.Set;

public class XUML_Set<E> {
	private Set<E> set;

	public XUML_Set(Set<E> set) {
		super();
		this.set = set;
	}

	public E any() {
		return set.iterator().next();
	}

	// public XUML_Set<E> filter(Predicate predicate) {
	// return (XUML_Set<E>)
	// super.stream().filter(predicate).collect(Collectors.toSet());
	// }

	// public XUML_Set<E> filter(Predicate predicate) {
	// return new XUML_Set<E>(this.set.stream().filter(predicate));
	// }
}
