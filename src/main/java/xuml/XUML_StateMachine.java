package xuml;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class XUML_StateMachine {

	private Set<XUML_State> states;
	private Map<String, XUML_Event> events;
	private XUML_State currentState;

	public XUML_StateMachine() {
		this.states = new HashSet<>();
		this.events = new HashMap<>();
	}

	public Set<XUML_State> getStates() {
		return states;
	}

	public Map<String, XUML_Event> getEvents() {
		return events;
	}

	public XUML_State getCurrentState() {
		return currentState;
	}

	public void setCurrentState(XUML_State currentState) {
		this.currentState = currentState;
	}

	public void addState(XUML_State state) {
		this.states.add(state);
	}

	public void addEvent(XUML_Event event) {
		this.events.put(event.getUniqueId() + event.getFrom().toString(), event);

	}

	public void setInitialState(XUML_State state) {
		if (!this.states.contains(state))
			throw new IllegalArgumentException("initial state is not in states");

		this.currentState = state;
	}

	public void processEvent(XUML_EventInstance eventInstance) {

	}

}
