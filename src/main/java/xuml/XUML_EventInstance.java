package xuml;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class XUML_EventInstance implements Delayed {
	private int eventNumber;
	private XUML_SignalReceiver receiver;
	private long endOfDelay;
	private boolean delayed;
	private Map<String, Object> parameters;

	public XUML_EventInstance(int eventNumber, XUML_SignalReceiver receiver,  long delay) {
		this.eventNumber = eventNumber;
		this.receiver = receiver;
		this.parameters = new HashMap<>();
		this.delayed = delay != 0;
		this.endOfDelay = delay * 1000000 + System.nanoTime();
	}

	// createAttr.signal(Person.TEST).setparam("test").send();

	public XUML_SignalReceiver setParam(String name, Object value) {
		parameters.put(name, value);
		return this.receiver;
	}

	public void send() {
		XUML_EventQueue.getInstance().generateEvent(this);

	}

	public int getEventNumber() {
		return this.eventNumber;
	}

	public XUML_SignalReceiver getReceiver() {
		return receiver;
	}

	public Map<String, Object> getParameters() {
		return this.parameters;
	}

	public boolean isDelayed() {
		return delayed;
	}

	@Override
	public int compareTo(Delayed o) {
		XUML_EventInstance other = (XUML_EventInstance) o;

		if (this.endOfDelay < other.endOfDelay)
			return 1;
		else if (this.endOfDelay > other.endOfDelay)
			return -1;
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		long tmp = unit.convert(this.endOfDelay - System.nanoTime(), TimeUnit.NANOSECONDS);
		return tmp;
	}
}
