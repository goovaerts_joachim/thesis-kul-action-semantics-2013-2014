package xuml;

import java.util.concurrent.DelayQueue;

public class XUML_EventQueue {
	private static XUML_EventQueue instance;
	private DelayQueue<XUML_EventInstance> events;

	private XUML_EventQueue() {
		super();
		this.events = new DelayQueue<>();

		new Thread() {
			public void run() {
				while (true) {
					XUML_EventInstance eventInstance;
					try {
						eventInstance = events.take();
						eventInstance.getReceiver().XUML_process(eventInstance);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}.start();
	}

	public static XUML_EventQueue getInstance() {
		if (instance == null)
			instance = new XUML_EventQueue();

		return instance;
	}

	public int size() {
		return this.events.size();

	}

	public void generateEvent(XUML_EventInstance xuml_EventInstance) {
		if (xuml_EventInstance.isDelayed())
			events.offer(xuml_EventInstance);
		else
			xuml_EventInstance.getReceiver().XUML_process(xuml_EventInstance);

	}
}
