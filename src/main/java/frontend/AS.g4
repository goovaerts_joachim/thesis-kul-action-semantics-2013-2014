grammar AS;

@header
{
	package frontend;
}

proc: exp+ EOF;

exp:  '(' exp ')' 												#parenExp
   | exp op=('=='|'!='|'<'|'=<'|'>'|'>=') exp 					#compExp 
   | exp op=('&&'|'||') exp 									#boolExp
   | '!'exp 													#negExp
   | 'empty 'exp 												#emptyExp
   | exp op='*' exp 											#multiplyExp
   | exp op=('+'|'-') exp 										#addExp
   | exp'.filter('ID '->' exp')'								#filterCollectionExp
   | exp'.map('ID '->' exp')'									#mapCollectionExp
   | exp'.all'													#allCollectionExp
   | exp'.any'													#anyCollectionExp
   | exp'.reduce('exp',''('ID','ID')' '->' exp')'				#reduceCollectionExp
   | ID'.'linkMethod=ID('_'Class)?'('object2=exp')' 	#linkExp
   | ID('.'ID)* 												#idExp
   | exp('.'ID)+ 												#idExp
   | (ids '=')+ exp		 										#assignExp  
   | exp'.signal('signal=ID(',' param)*')' 						#signalExp
   | 'signal('signal=ID(',' param)*')' 							#signalExp
   | exp'.delayedSignal('signal=ID','exp(',' param)*')' 		#delayedSignalExp
   | 'delayedSignal('signal=ID','exp(',' param)*')' 			#delayedSignalExp
   | Class'.create'('(' param (',' param)*')')? 				#createExp
   | exp'.delete' 												#deleteExp
   | exp'.reclassify('Class','Class')' 							#reclassifyExp
   | exp'.unlinkAll' 											#unlinkAllExp
   | object1=exp'.!'unlinkMethod=ID('_'Class)?'('object2=exp')'	#unlinkSingleExp
   | exp'.!'unlinkMethod=ID('_'Class)?'(*)'						#unlinkRelationExp
   | 'if' exp 'then' exp ('else' exp)? 							#condExp
   | Class 														#classCollectionExp
   | constExp 													#constExpr
   | '{' exp((','|NEWLINE)+ exp)* '}' 							#blockExp
   | 'print('exp')' 											#printExp
   | NEWLINE 													#newlineExp
   ;

ids : ID('.'ID)*;

param: ID ':=' exp;

constExp: StringLiteral | IntLiteral | BoolLiteral;

StringLiteral : '"' [a-zA-Z_0-9]* '"';
IntLiteral    : [0-9]+;
BoolLiteral   : 'true' | 'false';

Class : [A-Z] [a-zA-Z0-9]*;
ID  :   [a-zA-Z] [a-zA-Z0-9]*; 

NEWLINE:   '\r'? '\n' -> skip;
WS: [ \t\r\n]+ -> skip;

COMMENT : ( '//' ~[\r\n]* '\r'? '\n' | '/*' .*? '*/' ) -> channel(HIDDEN) ;
