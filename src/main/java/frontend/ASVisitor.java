// Generated from AS.g4 by ANTLR 4.1

	package frontend;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ASParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ASVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ASParser#constExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstExp(@NotNull ASParser.ConstExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(@NotNull ASParser.ParamContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#compExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompExp(@NotNull ASParser.CompExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#constExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstExpr(@NotNull ASParser.ConstExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#unlinkSingleExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnlinkSingleExp(@NotNull ASParser.UnlinkSingleExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#classCollectionExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassCollectionExp(@NotNull ASParser.ClassCollectionExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#reduceCollectionExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReduceCollectionExp(@NotNull ASParser.ReduceCollectionExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#deleteExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeleteExp(@NotNull ASParser.DeleteExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#idExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdExp(@NotNull ASParser.IdExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#linkExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLinkExp(@NotNull ASParser.LinkExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#unlinkRelationExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnlinkRelationExp(@NotNull ASParser.UnlinkRelationExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#unlinkAllExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnlinkAllExp(@NotNull ASParser.UnlinkAllExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#createExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateExp(@NotNull ASParser.CreateExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#allCollectionExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllCollectionExp(@NotNull ASParser.AllCollectionExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#reclassifyExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReclassifyExp(@NotNull ASParser.ReclassifyExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#proc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc(@NotNull ASParser.ProcContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#delayedSignalExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelayedSignalExp(@NotNull ASParser.DelayedSignalExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#negExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegExp(@NotNull ASParser.NegExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#signalExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignalExp(@NotNull ASParser.SignalExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#boolExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExp(@NotNull ASParser.BoolExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#ids}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIds(@NotNull ASParser.IdsContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#printExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintExp(@NotNull ASParser.PrintExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#multiplyExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplyExp(@NotNull ASParser.MultiplyExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#assignExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignExp(@NotNull ASParser.AssignExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#anyCollectionExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnyCollectionExp(@NotNull ASParser.AnyCollectionExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#mapCollectionExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMapCollectionExp(@NotNull ASParser.MapCollectionExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#condExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondExp(@NotNull ASParser.CondExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#addExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddExp(@NotNull ASParser.AddExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#emptyExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyExp(@NotNull ASParser.EmptyExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#filterCollectionExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilterCollectionExp(@NotNull ASParser.FilterCollectionExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#newlineExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewlineExp(@NotNull ASParser.NewlineExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#parenExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExp(@NotNull ASParser.ParenExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ASParser#blockExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockExp(@NotNull ASParser.BlockExpContext ctx);
}