package translation;

import model.Type;

public abstract class JavaNode {
	abstract protected Type getType();

	abstract protected String getPrefix();

	public abstract String getSuffix(boolean standalone);
}
