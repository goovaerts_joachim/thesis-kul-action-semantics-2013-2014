package translation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import model.Association;
import model.Attribute;
import model.Class;
import model.ClassDiagram;
import model.Type;

import org.antlr.v4.misc.Utils;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;

import utils.StringUtils;
import frontend.ASBaseVisitor;
import frontend.ASLexer;
import frontend.ASParser;
import frontend.ASParser.AddExpContext;
import frontend.ASParser.AllCollectionExpContext;
import frontend.ASParser.AnyCollectionExpContext;
import frontend.ASParser.AssignExpContext;
import frontend.ASParser.BlockExpContext;
import frontend.ASParser.BoolExpContext;
import frontend.ASParser.ClassCollectionExpContext;
import frontend.ASParser.CompExpContext;
import frontend.ASParser.CondExpContext;
import frontend.ASParser.ConstExpContext;
import frontend.ASParser.CreateExpContext;
import frontend.ASParser.DelayedSignalExpContext;
import frontend.ASParser.DeleteExpContext;
import frontend.ASParser.EmptyExpContext;
import frontend.ASParser.ExpContext;
import frontend.ASParser.FilterCollectionExpContext;
import frontend.ASParser.IdExpContext;
import frontend.ASParser.IdsContext;
import frontend.ASParser.LinkExpContext;
import frontend.ASParser.MapCollectionExpContext;
import frontend.ASParser.MultiplyExpContext;
import frontend.ASParser.NegExpContext;
import frontend.ASParser.ParamContext;
import frontend.ASParser.ParenExpContext;
import frontend.ASParser.PrintExpContext;
import frontend.ASParser.ProcContext;
import frontend.ASParser.ReduceCollectionExpContext;
import frontend.ASParser.SignalExpContext;
import frontend.ASParser.UnlinkRelationExpContext;
import frontend.ASParser.UnlinkSingleExpContext;
import generation.ActionLanguageGenerator;
import generation.AssociationClassGenerator;
import generation.AttributeGenerator;
import generation.ConstructorGenerator;
import generation.ImplementsGenerator;
import generation.MethodGenerator;
import generation.StateGenerator;
import generation.XmlParser;

public class JavaVisitor extends ASBaseVisitor<JavaNode> {
	private int freshId = 0;
	private Stack<Map<String, Type>> variables;
	private ClassDiagram classDiagram;
	private Class currentClass;

	public JavaVisitor(Map<String, Attribute> procedureParams,
			Class currentClass, ClassDiagram classDiagram) {
		super();
		this.classDiagram = classDiagram;
		this.currentClass = currentClass;

		this.variables = new Stack<>();
		Map<String, Type> attributes = new HashMap<String, Type>();

		for (Attribute attr : currentClass.getAttributes().values())
			attributes.put(attr.getName(), attr.getType());
		attributes.put("self", Type.getType(currentClass));
		attributes.put("null", Type.getNullType());
		this.variables.push(attributes);

		Map<String, Type> params = new HashMap<String, Type>();
		for (Attribute attr : procedureParams.values())
			params.put(attr.getName(), attr.getType());
		this.variables.push(params);
	}

	public Type lookupType(List<String> names, Type optionalType, int namesPos) {
		if (names.size() == namesPos)
			return optionalType;

		if (optionalType == null) {
			Type tempType = lookupType(names.get(namesPos));
			return lookupType(names, tempType, ++namesPos);
		} else {
			Type tempType = lookupAttribute(names.get(namesPos),
					optionalType.getType());
			return lookupType(names, tempType, ++namesPos);
		}
	}

	private Type lookupAttribute(String name, String className) {
		Class clas = this.classDiagram.getClasses().get(className);
		Attribute attribute = clas.getAttribute(name);

		if (attribute != null)
			return attribute.getType();

		if (!clas.getExtend().isEmpty())
			return lookupAttribute(name, clas.getExtend());

		throw new RuntimeException("lookup attribute: " + name);
	}

	public Type lookupType(String var) {
		for (int i = this.variables.size() - 1; i >= 0; i--)
			if (this.variables.get(i).containsKey(var))
				return this.variables.get(i).get(var);

		throw new IllegalArgumentException("variable " + var + " not found.");
	}

	public boolean isKnownVariable(String var) {
		for (int i = this.variables.size() - 1; i >= 0; i--)
			if (this.variables.get(i).containsKey(var))
				return true;

		return false;
	}

	private String getFullyQualifyRoleName(Class clas, String partialRolename) {
		for (Association ass : clas.getAssociations()) {
			String rolename = ass.getRollName1();
			if (rolename.equals(partialRolename))
				return partialRolename + "_" + ass.getClass1().getName();

			rolename = ass.getRollName2();
			if (rolename.equals(partialRolename))
				return partialRolename + "_" + ass.getClass2().getName();
		}

		if (!clas.getExtend().isEmpty())
			return getFullyQualifyRoleName(
					this.classDiagram.getClasses().get(clas.getExtend()),
					partialRolename);

		return partialRolename;
	}

	// visit methods

	@Override
	public JavaNode visitLinkExp(final LinkExpContext ctx) {
		//final JavaNode object1 = visit(ctx.object1);
		final Type object1Type = lookupType(ctx.ID(0).getText());
		final JavaNode object2 = visit(ctx.object2);

		final Class object1Class = this.classDiagram.getClasses().get(object1Type.getType());
		
		final String roleName = (ctx.Class() != null ? ctx.ID(1).getText() + "_"
				+ ctx.Class().getText() : getFullyQualifyRoleName(object1Class,
				ctx.ID(1).getText()));

		return new JavaNode() {
			@Override
			protected Type getType() {
				return object1Type;
			}

			@Override
			public String getSuffix(boolean standalone) {
				return ctx.ID(0) + "." + roleName + "("
						+ object2.getSuffix(false) + ");\n";
			}

			@Override
			protected String getPrefix() {
				return "";
			}
		};
	}

	@Override
	public JavaNode visitUnlinkSingleExp(UnlinkSingleExpContext ctx) {
		final JavaNode object1 = visit(ctx.object1);
		final JavaNode object2 = visit(ctx.object2);

		final Class object1Class = this.classDiagram.getClasses().get(
				object1.getType().getType());
		final String roleName = (ctx.Class() != null ? ctx.ID().getText() + "_"
				+ ctx.Class().getText() : getFullyQualifyRoleName(object1Class,
				ctx.ID().getText()));

		return new JavaNode() {
			@Override
			protected Type getType() {
				return object1.getType();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return object1.getSuffix(false) + "." + "unlink"
						+ StringUtils.capitalize(roleName) + "("
						+ object2.getSuffix(false) + ");\n";
			}

			@Override
			protected String getPrefix() {
				return object1.getPrefix() + "\n" + object2.getPrefix();
			}
		};
	}

	@Override
	public JavaNode visitUnlinkRelationExp(final UnlinkRelationExpContext ctx) {
		final JavaNode object1 = visit(ctx.exp());
		String roleName = ctx.ID().getText();

		final Class object1Class = this.classDiagram.getClasses().get(
				object1.getType().getType());
		final String fullyQuantifiedRoleName = (ctx.Class() != null ? roleName
				+ "_" + ctx.Class().getText() : getFullyQualifyRoleName(
				object1Class, roleName));

		String className = fullyQuantifiedRoleName
				.substring(fullyQuantifiedRoleName.lastIndexOf("_") + 1);
		final Class otherClass = this.classDiagram.getClasses().get(className);

		return new JavaNode() {
			@Override
			protected Type getType() {
				return object1.getType();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();

				List<String> ids = new ArrayList<>();
				ids.add(fullyQuantifiedRoleName);

				Type type = lookupType(ids, object1.getType(), 0);

				if (type.isCollection()) {
					sb.append("for(")
							.append(otherClass.getName())
							.append(" obj: ")
							.append(object1.getSuffix(false))
							.append(".get")
							.append(StringUtils
									.capitalize(fullyQuantifiedRoleName))
							.append("()){\n");
					sb.append(object1.getSuffix(false))
							.append(".unlink")
							.append(StringUtils
									.capitalize(fullyQuantifiedRoleName))
							.append("(obj);\n");
					sb.append("}");
				} else {
					sb.append(object1.getSuffix(false))
							.append(".unlink")
							.append(StringUtils
									.capitalize(fullyQuantifiedRoleName))
							.append("(").append(object1.getSuffix(false))
							.append(".").append("get").append(StringUtils.capitalize(fullyQuantifiedRoleName)).append("()")
							.append(");");
				}
				return sb.toString();
			}

			@Override
			protected String getPrefix() {
				return (object1.getPrefix().equals("") ? "" : object1
						.getPrefix() + "\n;");
			}
		};
	}

	@Override
	public JavaNode visitIdExp(final IdExpContext ctx) {
		final JavaNode exp = ctx.exp() != null ? visit(ctx.exp()) : null;
		Class clas = exp != null ? this.classDiagram.getClasses().get(
				exp.getType().getType()) : this.currentClass;

		final List<String> names = new ArrayList<>();

		for (TerminalNode idnode : ctx.ID()) {
			String id = getFullyQualifyRoleName(clas, idnode.getText());
			names.add(id);
		}

		final Type type = (exp == null) ? lookupType(names, null, 0)
				: lookupType(names, exp.getType(), 0);

		if (names.get(0).equals("self")) {
			names.remove(0);
			names.add(0, "this");
		}

		return new JavaNode() {
			@Override
			protected Type getType() {
				return type;
			}

			@Override
			protected String getPrefix() {
				return exp == null ? "" : exp.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();
				int startGetter = 0;

				if (exp == null) {
					sb.append(names.get(0));
					startGetter++;
				} else {
					sb.append(exp.getSuffix(false));
				}

				for (int i = startGetter; i < names.size(); i++)
					sb.append(".get" + StringUtils.capitalize(names.get(i))
							+ "()");

				if (standalone)
					sb.append(";");
				return sb.toString();
			}
		};
	}

	@Override
	public JavaNode visitParenExp(ParenExpContext ctx) {
		final JavaNode exp = visit(ctx.exp());

		return new JavaNode() {

			@Override
			protected Type getType() {
				return exp.getType();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return "(" + exp.getSuffix(standalone) + ")";
			}

			@Override
			protected String getPrefix() {
				return exp.getPrefix();
			}
		};
	}

	@Override
	public JavaNode visitAddExp(AddExpContext ctx) {
		final JavaNode exp1 = visit(ctx.exp(0));
		final JavaNode exp2 = visit(ctx.exp(1));
		final String operator = ctx.op.getText();

		if (!exp1.getType().equals(exp2.getType()))
			throw new RuntimeException("Type error (+/-)");

		if (!exp1.getType().equals(Type.getString())
				&& !exp1.getType().equals(Type.getInt()))
			throw new RuntimeException("Type error (+/-)");

		return createBinaryJavaNode(exp1, exp2, operator, exp1.getType());
	}

	@Override
	public JavaNode visitMultiplyExp(MultiplyExpContext ctx) {
		final JavaNode exp1 = visit(ctx.exp(0));
		final JavaNode exp2 = visit(ctx.exp(1));

		if (!exp1.getType().equals(exp2.getType()))
			throw new RuntimeException("Type error (*)");

		if (!exp1.getType().equals(Type.getInt()))
			throw new RuntimeException("Type error (*)");

		return createBinaryJavaNode(exp1, exp2, "*", exp1.getType());
	}

	@Override
	public JavaNode visitBoolExp(BoolExpContext ctx) {
		final JavaNode exp1 = visit(ctx.exp(0));
		final JavaNode exp2 = visit(ctx.exp(1));
		final String operator = ctx.op.getText();

		if (!exp1.getType().equals(exp2.getType()))
			throw new RuntimeException("Type error (&&/||)");

		if (!exp1.getType().equals(Type.getBool()))
			throw new RuntimeException("Type error (&&/||)");

		return createBinaryJavaNode(exp1, exp2, operator, exp1.getType());
	}

	@Override
	public JavaNode visitNegExp(NegExpContext ctx) {
		final JavaNode exp0 = visit(ctx.exp());

		if (!exp0.getType().equals(Type.getBool()))
			throw new RuntimeException("Type error (!)");

		return new JavaNode() {
			@Override
			protected Type getType() {
				return Type.getBool();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return (standalone ? "" : "!" + exp0.getSuffix(standalone));
			}

			@Override
			protected String getPrefix() {
				return exp0.getPrefix();
			}
		};
	}

	@Override
	public JavaNode visitCompExp(CompExpContext ctx) {
		final JavaNode exp1 = visit(ctx.exp(0));
		final JavaNode exp2 = visit(ctx.exp(1));
		final String operator = ctx.op.getText();

		if (!exp1.getType().equals(exp2.getType()))
			throw new RuntimeException("Type error (==|!=|<|=<|>|>=)");

		if (!exp1.getType().equals(Type.getInt()) && !operator.equals("==") && !operator.equals("!="))
			throw new RuntimeException(
					"Type error (not an Integer) (!=|<|=<|>|>=)");

		return createBinaryJavaNode(exp1, exp2, operator, Type.getBool());
	}

	@Override
	public JavaNode visitSignalExp(final SignalExpContext ctx) {
		final JavaNode dest = visit(ctx.exp());
		final Map<String, JavaNode> parameters = new HashMap<>();

		for (ParamContext pc : ctx.param()) {
			JavaNode param = visit(pc);
			parameters.put(pc.getText().substring(0, pc.getText().indexOf(":=")), param);
		}

		return new JavaNode() {

			@Override
			protected Type getType() {
				return dest.getType();
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder();
				sb.append(dest.getPrefix()).append("\n");

				for (Entry<String, JavaNode> entry : parameters.entrySet()) {
					sb.append(entry.getValue().getPrefix()).append(";\n");
				}

				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();
				sb.append(dest.getSuffix(false) + ".signal("
						+ dest.getType().getType() + "."
						+ ctx.signal.getText().toUpperCase() + ")");

				for (Entry<String, JavaNode> entry : parameters.entrySet()) {
					sb.append(".setParam(\"").append(entry.getKey()).append("\",")
							.append(entry.getValue().getSuffix(false))
							.append(")");
				}
				sb.append(".send();");
				return sb.toString();
			}

		};
	}

	
	@Override
	public JavaNode visitEmptyExp(final EmptyExpContext ctx) {
		final JavaNode exp = visit(ctx.exp());
		return new JavaNode() {

			@Override
			protected Type getType() {
				return Type.getBool();
			}

			@Override
			protected String getPrefix() {
				return exp.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return exp.getSuffix(false) + ".isEmpty()";
			}
			
		};
	}

	@Override
	public JavaNode visitDelayedSignalExp(final DelayedSignalExpContext ctx) {
		final JavaNode dest = visit(ctx.exp(0));
		final JavaNode delay = visit(ctx.exp(1));
		final Map<String, JavaNode> parameters = new HashMap<>();

		for (ParamContext pc : ctx.param()) {
			JavaNode param = visit(pc);
			parameters.put(pc.getText().substring(0, pc.getText().indexOf(":=")), param);
		}

		return new JavaNode() {

			@Override
			protected Type getType() {
				return dest.getType();
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder();
				sb.append(dest.getPrefix()).append("\n");

				for (Entry<String, JavaNode> entry : parameters.entrySet()) {
					sb.append(entry.getValue().getPrefix()).append(";\n");
				}

				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();
				sb.append(dest.getSuffix(false) + ".signal("
						+ dest.getType().getType() + "."
						+ ctx.signal.getText().toUpperCase() + ","
						+ delay.getSuffix(false) + ")");

				for (Entry<String, JavaNode> entry : parameters.entrySet()) {
					sb.append(".setParam(\"").append(entry.getKey()).append("\",")
							.append(entry.getValue().getSuffix(false))
							.append(")");
				}
				sb.append(".send();");
				return sb.toString();
			}

		};
	}

	private JavaNode createBinaryJavaNode(final JavaNode exp1,
			final JavaNode exp2, final String operator, final Type type) {
		final Type type1 = exp1.getType();

		return new JavaNode() {
			@Override
			protected Type getType() {
				return type;
			}

			@Override
			protected String getPrefix() {
				return exp1.getPrefix() + exp2.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				if (standalone)
					return "";
				if (type1.equals(Type.getString()) && operator.equals("=="))
					return (exp1.getSuffix(false) + ".equals(" + exp2
							.getSuffix(false)) + ")";
				else
					return (exp1.getSuffix(false) + " " + operator + " " + exp2
							.getSuffix(false));
			}
		};
	}

	@Override
	public JavaNode visitProc(ProcContext ctx) {
		final List<JavaNode> exps = new ArrayList<>();
		for (ExpContext expContext : ctx.exp())
			exps.add(visit(expContext));
		return new JavaNode() {
			@Override
			protected Type getType() {
				throw new RuntimeException();
			}

			@Override
			protected String getPrefix() {
				return "";
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();
				for (JavaNode exp : exps) {
					sb.append(exp.getPrefix());
					sb.append(exp.getSuffix(true));
				}
				return sb.toString();
			}
		};
	}

	@Override
	public JavaNode visitPrintExp(PrintExpContext ctx) {
		final int tempId = freshId++;
		final JavaNode exp = visit(ctx.exp());

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp.getType();
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder(exp.getPrefix());
				sb.append(
						exp.getType() + " temp" + tempId + " = "
								+ exp.getSuffix(false)).append(";\n");
				sb.append("System.out.println(temp" + tempId + ")").append(
						";\n");
				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return (standalone ? "" : "temp" + tempId);
			}
		};
	}

	@Override
	public JavaNode visitConstExp(ConstExpContext ctx) {
		final Type type = ctx.StringLiteral() != null ? Type.getString() : (ctx
				.IntLiteral() != null ? Type.getInt() : Type.getBool());
		final String text = ctx.getText();

		return new JavaNode() {
			@Override
			protected Type getType() {
				return type;
			}

			@Override
			protected String getPrefix() {
				return "";
			}

			@Override
			public String getSuffix(boolean standalone) {
				return (standalone ? "" : text);
			}
		};
	}

	@Override
	public JavaNode visitAssignExp(AssignExpContext ctx) {
		final int tempId = freshId++;
		final JavaNode exp = visit(ctx.exp());
		final List<IdsContext> ids = ctx.ids();
		final Set<IdsContext> notYetDeclared = new HashSet<>();

		for (IdsContext idList : ids) {
			if (idList.ID().size() == 1) {
				if (!isKnownVariable(idList.ID(0).getText())) {
					variables.peek().put(idList.ID(0).getText(), exp.getType());
					notYetDeclared.add(idList);
				}
			}
		}

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp.getType();
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder(exp.getPrefix());
				sb.append(
						exp.getType() + " temp" + tempId + " = "
								+ exp.getSuffix(false)).append(";\n");

				for (IdsContext idList : ids) {
					if (idList.ID().size() == 1) {
						if (notYetDeclared.contains(idList)) {
							sb.append(exp.getType().toString()).append(" ");
						}
						sb.append(idList.ID(0)).append(" = ")
								.append("temp" + tempId).append(";\n");
					} else {
						sb.append(idList.ID(0));
						for (int i = 1; i < idList.ID().size() - 1; i++)
							sb.append(".get")
									.append(Utils.capitalize(idList.ID(i)
											.getText())).append("()");
						sb.append(".set")
								.append(Utils.capitalize(idList.ID(
										idList.ID().size() - 1).getText()))
								.append("(");
						sb.append("temp").append(tempId).append(");\n");
					}
				}

				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return (standalone ? "" : "temp" + tempId);
			}
		};
	}

	@Override
	public JavaNode visitBlockExp(BlockExpContext ctx) {
		final int tempId = freshId++;
		final List<JavaNode> exps = new ArrayList<>();

		for (ExpContext exp : ctx.exp())
			exps.add(visit(exp));
		final JavaNode finalExp = exps.get(exps.size() - 1);
		exps.remove(exps.size() - 1);

		final Type type = finalExp.getType();

		return new JavaNode() {
			@Override
			protected Type getType() {
				return type;
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder();

				for (JavaNode exp : exps)
					sb.append(exp.getPrefix()).append(exp.getSuffix(true));
				sb.append(finalExp.getPrefix());
				sb.append(
						finalExp.getType() + " temp" + tempId + " = "
								+ finalExp.getSuffix(false)).append(";\n");

				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return (standalone ? "" : "temp" + tempId);
			}
		};
	}

	@Override
	public JavaNode visitCreateExp(CreateExpContext ctx) {
		final String className = ctx.Class().getText();
		final TreeMap<String, JavaNode> params = new TreeMap<>();

		for (ParamContext pc : ctx.param())
			params.put(pc.ID().getText(), visit(pc));

		return new JavaNode() {
			@Override
			protected Type getType() {
				return Type.getType(className);
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder();
				for (String param : params.keySet())
					sb.append(params.get(param).getPrefix());
				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder(className + ".create(");
				List<String> paramTexts = new ArrayList<>();
				for (String param : params.keySet())
					paramTexts.add(params.get(param).getSuffix(false));
				sb.append(StringUtils.join(paramTexts, ","));
				sb.append(")").append(standalone ? ";" : "");
				return sb.toString();
			}
		};
	}

	@Override
	public JavaNode visitDeleteExp(DeleteExpContext ctx) {
		final JavaNode exp = visit(ctx.exp());

		return new JavaNode() {
			@Override
			protected Type getType() {
				return Type.getBool();
			}

			@Override
			protected String getPrefix() {
				return exp.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return exp.getSuffix(false) + ".delete()"
						+ (standalone ? ";" : "");
			}
		};
	}

	@Override
	public JavaNode visitParam(ParamContext ctx) {
		final JavaNode exp = visit(ctx.exp());

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp.getType();
			}

			@Override
			protected String getPrefix() {
				return exp.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return exp.getSuffix(standalone);
			}
		};
	}

	@Override
	public JavaNode visitCondExp(CondExpContext ctx) {
		final int tempId = freshId++;
		final JavaNode exp0 = visit(ctx.exp(0));
		final JavaNode exp1 = visit(ctx.exp(1));
		final boolean elsePart = ctx.exp().size() > 2;
		final JavaNode exp2 = elsePart ? visit(ctx.exp(2)) : null;

		if (elsePart && !exp1.getType().equals(exp2.getType()))
			throw new RuntimeException("Type error (if)");

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp1.getType();
			}

			@Override
			protected String getPrefix() {
				StringBuilder sb = new StringBuilder();
				sb.append(exp1.getType() + " temp" + tempId + " = null;\n");
				sb.append(exp0.getPrefix());
				sb.append("if(").append(exp0.getSuffix(false)).append(") {\n");
				sb.append(exp1.getPrefix());
				sb.append("temp" + tempId + " = " + exp1.getSuffix(false)
						+ ";\n");
				sb.append("}\n");
				if (elsePart) {
					sb.append("else {\n");
					sb.append(exp2.getPrefix());
					sb.append("temp" + tempId + " = " + exp2.getSuffix(false)
							+ ";\n}\n");
				}
				return sb.toString();
			}

			@Override
			public String getSuffix(boolean standalone) {
				return (standalone ? "" : "temp" + tempId);
			}
		};
	}

	private void declareVariable(String var, Type type) {
		Map<String, Type> newScope = new HashMap<>();
		newScope.put(var, type);
		this.variables.push(newScope);
	}

	private void undeclareVariable() {
		this.variables.pop();
	}

	@Override
	public JavaNode visitFilterCollectionExp(
			final FilterCollectionExpContext ctx) {
		final JavaNode exp0 = visit(ctx.exp(0));
		declareVariable(ctx.ID().getText(), exp0.getType().getInnerType());
		final JavaNode exp1 = visit(ctx.exp(1));

		if (!exp1.getType().equals(Type.getBool()))
			throw new RuntimeException(
					"Type error (filter :: lambda expression should be boolean)");

		undeclareVariable();

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp0.getType();
			}

			@Override
			protected String getPrefix() {
				return exp0.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();

				// TODO terrible fix to eliminate multiple collects
				String exp0rep = exp0.getSuffix(false);
				int index = exp0rep
						.lastIndexOf(".collect(Collectors.toList())");
				if (index != -1)
					exp0rep = exp0rep.substring(0, index);

				sb.append(exp0rep);
				
				if(!exp0rep.contains(".stream()"))
					sb.append(".stream()");
				
				sb.append(".filter(\n");
				sb.append(ctx.ID().getText() + " -> {");
				sb.append(exp1.getPrefix()).append("\n");
				sb.append("return ").append(exp1.getSuffix(false))
						.append(";})");
				sb.append(".collect(Collectors.toList())");
				return sb.toString();
			}
		};
	}

	@Override
	public JavaNode visitMapCollectionExp(final MapCollectionExpContext ctx) {
		final JavaNode exp0 = visit(ctx.exp(0));
		declareVariable(ctx.ID().getText(), exp0.getType().getInnerType());
		final JavaNode exp1 = visit(ctx.exp(1));
		undeclareVariable();

		final Type type = Type.getCollectionType(exp1.getType().getType());

		return new JavaNode() {
			@Override
			protected Type getType() {
				return type;
			}

			@Override
			protected String getPrefix() {
				return exp0.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();

				// TODO terrible fix to eliminate multiple collects
				String exp0rep = exp0.getSuffix(false);
				int index = exp0rep
						.lastIndexOf(".collect(Collectors.toList())");
				if (index != -1)
					exp0rep = exp0rep.substring(0, index);
				
				sb.append(exp0rep);
				
				if(!exp0rep.contains(".stream()"))
					sb.append(".stream()");
				
				sb.append(".map(\n");
				sb.append(ctx.ID().getText() + " -> {");
				sb.append(exp1.getPrefix()).append("\n");
				sb.append("return ").append(exp1.getSuffix(false))
						.append(";})");
				sb.append(".collect(Collectors.toList())");
				return sb.toString();
			}
		};
	}

	@Override
	public JavaNode visitReduceCollectionExp(
			final ReduceCollectionExpContext ctx) {
		final JavaNode exp0 = visit(ctx.exp(0));
		final JavaNode exp1 = visit(ctx.exp(1));
		Map<String, Type> types = new HashMap<>();
		types.put(ctx.ID(0).getText(), exp0.getType().getInnerType());
		types.put(ctx.ID(1).getText(), exp0.getType().getInnerType());
		this.variables.push(types);
		final JavaNode lambdaExp = visit(ctx.exp(2));
		undeclareVariable();

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp1.getType();
			}

			@Override
			protected String getPrefix() {
				return exp0.getPrefix() + exp1.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				StringBuilder sb = new StringBuilder();

				// TODO terrible fix to eliminate multiple collects
				String exp0rep = exp0.getSuffix(false);
				int index = exp0rep
						.lastIndexOf(".collect(Collectors.toList())");
				if (index != -1)
					exp0rep = exp0rep.substring(0, index);

				sb.append(exp0rep);
				
				if(!exp0rep.contains(".stream()"))
					sb.append(".stream()");
				
				sb.append(".reduce(").append(exp1.getSuffix(false)).append(",");
				sb.append("(" + ctx.ID(0) + "," + ctx.ID(1) + ") -> {\n");
				sb.append(lambdaExp.getPrefix());
				sb.append("return " + lambdaExp.getSuffix(false) + ";})");

				return sb.toString();
			}
		};
	}

	@Override
	public JavaNode visitAllCollectionExp(AllCollectionExpContext ctx) {
		final JavaNode exp0 = visit(ctx.exp());

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp0.getType();
			}

			@Override
			protected String getPrefix() {
				return exp0.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				// TODO terrible fix to eliminate multiple collects
				String exp0rep = exp0.getSuffix(false);
				int index = exp0rep
						.lastIndexOf(".collect(Collectors.toList())");
				if (index != -1)
					exp0rep = exp0rep.substring(0, index);

				return exp0rep + ".collect(Collectors.toList())";
			}
		};
	}

	@Override
	public JavaNode visitAnyCollectionExp(AnyCollectionExpContext ctx) {
		final JavaNode exp0 = visit(ctx.exp());

		return new JavaNode() {
			@Override
			protected Type getType() {
				return exp0.getType().getInnerType();
			}

			@Override
			protected String getPrefix() {
				return exp0.getPrefix();
			}

			@Override
			public String getSuffix(boolean standalone) {
				// TODO terrible fix to eliminate multiple collects
				String exp0rep = exp0.getSuffix(false);
				int index = exp0rep
						.lastIndexOf(".collect(Collectors.toList())");
				if (index != -1)
					exp0rep = exp0rep.substring(0, index);

				return exp0rep + ".findAny().get()";
			}
		};
	}

	@Override
	public JavaNode visitClassCollectionExp(final ClassCollectionExpContext ctx) {
		return new JavaNode() {
			@Override
			protected Type getType() {
				return Type.getCollectionType(ctx.Class().getText());
			}

			@Override
			public String getSuffix(boolean standalone) {
				return standalone ? "" : ctx.Class().getText() + ".stream()";
			}

			@Override
			protected String getPrefix() {
				return "";
			}
		};
	}

	public static void main(String args[]) throws IOException {
		XmlParser xmlparser = new XmlParser("model3Multiplicity.xml");
		ClassDiagram classDiagram = xmlparser.parse();
		ImplementsGenerator implGen = new ImplementsGenerator();
		AssociationClassGenerator assGen = new AssociationClassGenerator();
		AttributeGenerator attrGen = new AttributeGenerator();
		ConstructorGenerator consGen = new ConstructorGenerator();
		ActionLanguageGenerator alGen = new ActionLanguageGenerator();
		MethodGenerator methGen = new MethodGenerator();
		StateGenerator stateGen = new StateGenerator();
		implGen.setNext(assGen);
		assGen.setNext(attrGen);
		attrGen.setNext(consGen);
		consGen.setNext(alGen);
		alGen.setNext(methGen);
		methGen.setNext(stateGen);
		implGen.handle(classDiagram);
		// InputStream is =
		// ClassLoader.getSystemResourceAsStream("testNotForCommit.gr");
		InputStream is = ClassLoader
				.getSystemResourceAsStream("testNotForCommit.gr");
		CharStream cs = new ANTLRInputStream(is);
		ASLexer lexer = new ASLexer(cs);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		ASParser parser = new ASParser(tokens);
		parser.setBuildParseTree(true);
		ASParser.ProcContext tree = parser.proc();
		JavaVisitor builder = new JavaVisitor(new HashMap<String, Attribute>(),
				classDiagram.getClasses().get("School"), classDiagram);
		JavaNode java = builder.visit(tree);
		System.out.println(java.getSuffix(true));
	}
}
