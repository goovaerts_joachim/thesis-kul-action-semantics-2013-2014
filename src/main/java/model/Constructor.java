package model;

import java.util.Set;

import org.abego.treelayout.internal.util.java.lang.string.StringUtil;

import utils.StringUtils;

public class Constructor {
	private Visibility visibility;
	private String name;
	private Set<Attribute> parameters;
	private String procedure;

	public Constructor(Visibility visibility, String name, Set<Attribute> parameters, String procedure) {
		super();
		this.name = name;
		this.parameters = parameters;
		this.visibility = visibility;
		this.procedure = procedure;
	}

	@Override
	public String toString() {
		String constructor = this.visibility.getValue() + " " + this.name;
		constructor += "(";
		
		constructor += StringUtils.join(this.parameters, ",");
		
//		for (Attribute attr : this.parameters)
//			constructor += attr + ", ";
//
//		if (!this.parameters.isEmpty())
//			constructor = constructor.substring(0, constructor.length() - 2);

		constructor += ") {";

		constructor += "super();\n";

		constructor += procedure;

		constructor += "}";

		return constructor;
	}

}
