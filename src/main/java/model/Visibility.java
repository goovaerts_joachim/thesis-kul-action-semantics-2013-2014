package model;

public enum Visibility {
	PUBLIC("public"), PROTECTED("protected"), PRIVATE("private"), NONE("");

	private final String value;

	Visibility(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
