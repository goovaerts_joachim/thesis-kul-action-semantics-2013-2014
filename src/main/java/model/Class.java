package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.state.StateMachine;

public class Class {
	private Visibility visibility;
	private String name;
	private String extend;
	private Set<String> implement;
	private Map<String, Attribute> attributes;
	private StateMachine stateMachine;
	private Set<Association> associations;
	private Map<String,Method> methods;
	private Map<String, Class> innerClasses;
	private Set<Constructor> constructors;
	private boolean isXumlClass = false;
	private Set<String> imports;

	public Class(Visibility visibility, String name, String extend, Set<String> imp, Map<String, Attribute> attr, StateMachine sm, Map<String,Method> methods) {
		this.visibility = visibility;
		this.name = name;
		this.extend = extend;
		this.implement = imp;
		this.attributes = attr;
		this.stateMachine = sm;

		this.associations = new HashSet<>();
		this.methods = methods;
		this.innerClasses = new HashMap<>();
		this.constructors = new HashSet<>();
		this.imports = new HashSet<>();
	}

	public Class(Visibility visibility, String name, String extend, Set<String> imp, Map<String, Attribute> attr) {
		this(visibility, name, extend, imp, attr, null,new HashMap<String,Method>());
	}

	public void addAssociation(Association ass) {
		this.associations.add(ass);
	}

	public void addImplement(String implement) {
		this.implement.add(implement);
	}

	public void addAttribute(Attribute attribute) {
		this.attributes.put(attribute.getName(), attribute);
	}

	public void addMethod(Method method) {
		this.methods.put(method.getName(), method);
	}

	public void addInnerClass(Class innerClass) {
		this.innerClasses.put(innerClass.getName(), innerClass);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		generateClass(sb);

		return sb.toString();
	}

	private void generateClass(StringBuilder sb) {
		generateImportStatements(sb);
		
		if (this.extend.isEmpty()) {
			sb.append(String.format("%s class %s %s {\n", this.visibility.getValue(), this.name, generateImplementsCode()));
		} else {
			sb.append(String.format("%s class %s extends %s %s{\n", this.visibility.getValue(), this.name, this.extend, generateImplementsCode()));
		}

		generateAttributesCode(sb);
		generateConstructorsCode(sb);
		generateMethodsCode(sb);
		generateInnerClassesCode(sb);

		sb.append("}");
	}

	private String generateImplementsCode() {
		String implement = "";

		if (!this.implement.isEmpty()) {
			implement += " implements ";

			for (String imp : this.implement) {
				implement += imp + ", ";
			}

			return implement.substring(0, implement.length() - 2);
		}
		return implement;
	}

	private void generateInnerClassesCode(StringBuilder sb) {
		for (Class inner : this.innerClasses.values()) {
			sb.append(inner);
		}
	}

	private void generateMethodsCode(StringBuilder sb) {
		for (Method method : this.methods.values()) {
			sb.append(method).append("\n\n");
		}
	}

	private void generateConstructorsCode(StringBuilder sb) {
		for (Constructor constructor : this.constructors) {
			sb.append(constructor);
		}

		sb.append("\n\n");
	}

	private void generateAttributesCode(StringBuilder sb) {
		for (Attribute att : this.attributes.values())
			sb.append(att).append(";\n");

		sb.append("\n\n");
	}

	private void generateImportStatements(StringBuilder sb) {
		for(String impor: this.imports) {
			sb.append("import ").append(impor).append(";\n");
		}

		sb.append("\n\n");
	}

	// getters and setters

	public String getName() {
		return name;
	}

	public String getExtend() {
		return extend;
	}

	public Set<String> getImports() {
		return implement;
	}

	public Map<String, Attribute> getAttributes() {
		return attributes;
	}

	public Attribute getAttribute(String name) {
		return this.attributes.get(name);
	}

	public StateMachine getStateMachine() {
		return stateMachine;
	}

	public Set<Association> getAssociations() {
		return associations;
	}

	public Map<String, Class> getInnerClasses() {
		return innerClasses;
	}

	public void addConstructor(Constructor constructor) {
		this.constructors.add(constructor);
	}

	public boolean isXumlClass() {
		return isXumlClass;
	}

	public void setXumlClass(boolean isXumlClass) {
		this.isXumlClass = isXumlClass;
	}

	public Map<String,Method> getMethods() {
		return methods;
	}
	
	public boolean hasSuperClass() {
		return !this.extend.equals("");
	}

	public void addImport(String importName) {
		this.imports.add(importName);
	}

	
	
}
