package model;

public class Association {
	private String name;
	private Class class1;
	private Class class2;
	private String multiplicity1;
	private String multiplicity2;
	private String rollName1;
	private String rollName2;

	public Association(String name, Class class1, Class class2, String multiplicity1, String multiplicity2, String rollName1, String rollName2) {
		super();
		this.name = name;
		this.class1 = class1;
		this.class2 = class2;
		this.multiplicity1 = multiplicity1;
		this.multiplicity2 = multiplicity2;
		this.rollName1 = rollName1;
		this.rollName2 = rollName2;

		class1.addAssociation(this);
		class2.addAssociation(this);
	}

	public void setClass1(Class class1) {
		this.class1 = class1;
	}

	public void setClass2(Class class2) {
		this.class2 = class2;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRollName1(String rollName1) {
		this.rollName1 = rollName1;
	}

	public void setRollName2(String rollName2) {
		this.rollName2 = rollName2;
	}

	public void setMultiplicity1(String multiplicity1) {
		this.multiplicity1 = multiplicity1;
	}

	public void setMultiplicity2(String multiplicity2) {
		this.multiplicity2 = multiplicity2;
	}

	public Class getClass1() {
		return class1;
	}

	public Class getClass2() {
		return class2;
	}

	public String getName() {
		return name;
	}

	public String getRollName1() {
		return rollName1;
	}

	public String getRollName2() {
		return rollName2;
	}

	public String getMultiplicity1() {
		return multiplicity1;
	}

	public String getMultiplicity2() {
		return multiplicity2;
	}

	public Class getOtherClass(Class clazz) {
		return (clazz == this.class1 ? this.class2 : this.class1);
	}

	public String getOtherRollName(String rollName) {
		return (rollName.equals(this.rollName1) ? this.rollName2 : this.rollName1);
	}

	public String getCorrespondingMultiplicity(String roleName) {
		return (roleName.equals(this.rollName1) ? this.multiplicity1 : this.multiplicity2);
	}
}
