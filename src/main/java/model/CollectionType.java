package model;

public class CollectionType extends Type {
	private String collectionName;
	private String collectionImpl;
	private String secondType;

	CollectionType(String type, String collection, String collectionImpl) {
		super(type);
		this.collectionName = collection;
		this.collectionImpl = collectionImpl;
		this.collection = true;
		this.innerType = Type.getType(type);
	}

	CollectionType(String type, String secondType, String collection, String collectionImpl) {
		super(type);
		this.collectionName = collection;
		this.collectionImpl = collectionImpl;
		this.secondType = secondType;
		this.collection = true;
		this.innerType = Type.getType(type);
	}

	public String getCollection() {
		return collectionName;
	}

	public String getSecondType() {
		if (this.secondType != null && !this.secondType.isEmpty())
			return this.secondType;
		return this.type;
	}
	
	public boolean isCollection() {
		return true;
	}

	@Override
	public String toString() {
		return this.collectionName + getGenerics(this.type, this.secondType);
	}

	@Override
	public String getDeclImpl(String varName) {
		return this.collectionName + getGenerics(this.type, this.secondType) + " " + varName + " = new " + this.collectionImpl
				+ getGenerics(this.type, this.secondType) + "()";
	}

	private String getGenerics(String type, String secondType) {
		return "<" + this.type + (this.secondType != null && !this.secondType.isEmpty() ? "," + this.secondType + ">" : ">");
	}

	// factory methods
	public static Type getMap(String keyType, String valueType) {
		return new CollectionType(keyType, valueType, "Map", "Hashmap");
	}

	public static Type getSet(String type) {
		return new CollectionType(type, "Set", "HashSet");
	}

	public static Type getList(String type) {
		return new CollectionType(type, "List", "ArrayList");
	}
}