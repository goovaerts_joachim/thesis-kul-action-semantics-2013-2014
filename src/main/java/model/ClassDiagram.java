package model;

import java.util.Map;
import java.util.Set;

public class ClassDiagram {
	// private Set<Class> classes;
	private Map<String, Class> classes;
	private Map<String,Association> associations;
	private Map<String,AssociationClass> associationClasses;

	public ClassDiagram(Map<String, Class> classes2, Map<String, Association> associations2, Map<String, AssociationClass> associationsClasses) {
		this.classes = classes2;
		this.associations = associations2;
		this.associationClasses = associationsClasses;
	}

	public Map<String,Association> getAssociations() {
		return associations;
	}

	public Map<String, Class> getClasses() {
		return classes;
	}

	public Map<String, AssociationClass> getAssociationClasses() {
		return associationClasses;
	}
}
