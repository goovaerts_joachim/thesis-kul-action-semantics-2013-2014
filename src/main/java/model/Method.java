package model;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import utils.StringUtils;

public class Method {
	private String name;
	private Visibility visibility;
	private Type returnType;
	private Map<String,Attribute> parameters;
	private String code;
	private boolean isStatic = false;
	private boolean isUmlSpecified = false;

	public Method(String name, Visibility visibility, Type returnType, Map<String,Attribute> attributes, String code) {
		super();
		this.name = name;
		this.visibility = visibility;
		this.returnType = returnType;
		this.parameters = attributes;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public Map<String,Attribute> getAttributes() {
		return parameters;
	}

	public String getCode() {
		return code;
	}

	public void setIsStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}
	
	public boolean isUmlSpecified() {
		return isUmlSpecified;
	}

	public void setUmlSpecified(boolean isUmlSpecified) {
		this.isUmlSpecified = isUmlSpecified;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Type getReturnType() {
		return returnType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.visibility.getValue()).append(" ");
		sb.append(this.isStatic ? "static " : "");
		sb.append(this.returnType).append(" ");
		sb.append(this.name).append("(");
		sb.append(generateParameters()).append(") {\n");
		sb.append(this.code).append("\n}");

		return sb.toString();
		// return String.format("%s %s %s %s(%s) {\n%s\n}",
		// this.visibility.getValue(),(this.isStatic? "static":""),
		// this.returnType.getValue(),
		// this.name, generateParameters(), this.code);
	}

	private String generateParameters() {
		if (this.parameters.isEmpty())
			return "";
		
		return StringUtils.join(this.parameters.values(), " ,");
	}

}
