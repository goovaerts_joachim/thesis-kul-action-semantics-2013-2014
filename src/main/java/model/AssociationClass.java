package model;

import java.util.Map;
import java.util.Set;

import model.state.StateMachine;

public class AssociationClass extends Class {
	private Association association;
	
	public AssociationClass(Visibility visibility, String name, String extend, Set<String> imp, Map<String,Attribute> attr,StateMachine sm, Map<String,Method> methods) {
		super(visibility, name, extend, imp, attr, sm, methods);
	}

	public Association getAssociation() {
		return association;
	}

	public void setAssociation(Association association) {
		this.association = association;
	}
	
}
