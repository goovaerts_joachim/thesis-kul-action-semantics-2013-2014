package model;

public class Type {
	protected String type;
	protected Type innerType;
	protected boolean collection;
	
	Type(String type) {
		this.type = type;
	}
	
	Type(Type type){
		this.innerType = type;
		this.type = "List<" + type + ">";
		this.collection = true;
	}
	
	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return type;
	}
	
	public String getDeclImpl(String varName) {
		return type + " " + varName + " = new " + type + "()";
	}
	
	public boolean isCollection() {
		return false;
	}
	
	public Type getInnerType() {
		if (!collection) throw new RuntimeException();
		return innerType;
	}

	// factory methods
	public static Type getCollectionType(String string){
		return new Type(Type.getType(string));
	}
	
	public static Type getInt() {
		return new Type("Integer");
	}

	public static Type getBool() {
		return new Type("Boolean");
	}

	public static Type getString() {
		return new Type("String");
	}
	
	public static Type getDate() {
		return new Type("Date");
	}

	public static Type getEventInstance() {
		return new Type("XUML_EventInstance");
	}

	public static Type getVoid() {
		return new Type("void");
	}
	
	public static Type getPredicate() {
		return new Type("Predicate");
	}
	
	//TODO refactor name
	public static Type getType(Class clas) {
		return new Type(clas.getName());
	}
	
	public static Type getStateMachine() {
		return new Type("XUML_StateMachine");
	}
	
	public static Type getType(String string) {
		return new Type(string);
	}
	public static Type getNullType() {
		return new Type("null");
	}
	
	public String defaultValue() {		
		if (type.equals(Type.getInt())) return "-1";
		else if (type.equals(Type.getBool())) return "false";
		else return "null";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if(this.getType().equals("null") || ((Type) obj).getType().equals("null")) return true;
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Type other = (Type) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}


	
	
}
