package model;


public class Attribute {
	private String name;
	private Type type;
	private Visibility visibility;
	private boolean isStatic;
	private boolean isFinal;
	private String initialValue;

	public Attribute(String name, Type type, Visibility visibility, boolean isStatic, boolean isFinal, String initialValue) {
		this.name = name;
		this.type = type;
		this.visibility = visibility;
		this.isFinal = isFinal;
		this.isStatic = isStatic;
		this.initialValue = initialValue;
	}

	public Attribute(String name, Type type, Visibility visibility, boolean isStatic, boolean isFinal) {
		this.name = name;
		this.type = type;
		this.visibility = visibility;
		this.isFinal = isFinal;
		this.isStatic = isStatic;
		initialValue = "";
	}

	public Attribute(String name, Type type, Visibility visibility) {
		this(name, type, visibility, false, false);

	}

	public Attribute(String name, Type type) {
		this(name, type, Visibility.NONE);
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setInitialValue(String value) {
		this.initialValue = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.visibility.getValue()).append(" "); // visibibility
		sb.append(isStatic ? "static " : ""); // static
		sb.append(isFinal ? "final " : ""); // final
		sb.append(this.type).append(" "); // type
		sb.append(this.name).append(" "); // name
		sb.append(!this.initialValue.isEmpty() ? " = " + this.initialValue : ""); // initial
																					// value
		//sb.append(";");

		return sb.toString();
		// return this.visibility.getValue() + " " + (isStatic ? "static ":"") +
		// (isFinal?"final ":"") + this.type.getValue() + " " + this.name +
		// (!this.initialValue.isEmpty()? " = " + this.initialValue:"") +";";
	}

	public boolean isStatic() {
		return this.isStatic;
	}

}
