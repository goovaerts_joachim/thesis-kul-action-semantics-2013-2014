package model.state;

import java.util.Map;

public class StateMachine {

	private Map<String, State> states;
	private Map<String, Event> events;
	private State defaultState;

	public StateMachine(Map<String, State> states, Map<String, Event> events, String defaultState) {
		super();
		this.states = states;
		this.events = events;
		this.defaultState = this.states.get(defaultState);
	}

	public Map<String, State> getStates() {
		return states;
	}

	public Map<String, Event> getEvents() {
		return events;
	}

	public State getDefaultState() {
		return defaultState;
	}
}
