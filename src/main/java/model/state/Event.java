package model.state;

public class Event {
	private String name;
	private State from;
	private State to;
	private int uniqueId = -1;

	public Event(String name, State from, State to) {
		super();
		this.name = name;
		this.from = from;
		this.to = to;

		from.addoutgoingEvent(this);
		to.addIncomingEvent(this);
	}

	public Event(String name, State from, State to, int uniqueId) {
		this(name,from,to);
		this.uniqueId = uniqueId;
	}

	public String getName() {
		return name;
	}

	public State getFrom() {
		return from;
	}

	public State getTo() {
		return to;
	}

	public int getUniqueId() {
		return this.uniqueId;
	}

	public void setuniqueId(int id) {
		this.uniqueId = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

}
