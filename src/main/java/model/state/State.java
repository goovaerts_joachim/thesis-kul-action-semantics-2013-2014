package model.state;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Attribute;
import model.Type;

public class State {
	private String name;
	private int number;
	private String actionLanguageProcedure;
	private String javaProcedure;
	private Set<Event> incomingEvents;
	private Set<Event> outgoingEvents;
	private Map<String,Attribute> parameters;
	
	public State(String name, int number, String actionLanguageProcedure, Map<String, Attribute> parameters) {
		super();
		this.name = name;
		this.number = number;
		this.actionLanguageProcedure = actionLanguageProcedure;
		this.incomingEvents = new HashSet<>();
		this.outgoingEvents = new HashSet<>();
		this.parameters=parameters;
	}
	
	public void addIncomingEvent(Event e){
		this.incomingEvents.add(e);
	}
	
	public void addoutgoingEvent(Event e){
		this.outgoingEvents.add(e);
	}

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public String getActionLanguageProcedure() {
		return actionLanguageProcedure;
	}

	public Set<Event> getIncomingEvents() {
		return incomingEvents;
	}

	public Set<Event> getOutgoingEvents() {
		return outgoingEvents;
	}
	
	

	public String getJavaProcedure() {
		return javaProcedure;
	}

	public void setJavaProcedure(String javaProcedure) {
		this.javaProcedure = javaProcedure;
	}
	

	public Map<String, Attribute> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Attribute> parameters) {
		this.parameters = parameters;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + number;
		result = prime * result
				+ ((actionLanguageProcedure == null) ? 0 : actionLanguageProcedure.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number != other.number)
			return false;
		if (actionLanguageProcedure == null) {
			if (other.actionLanguageProcedure != null)
				return false;
		} else if (!actionLanguageProcedure.equals(other.actionLanguageProcedure))
			return false;
		return true;
	}

	
	
}
