package generation;

import utils.StringUtils;
import model.Association;
import model.Attribute;
import model.Class;
import model.ClassDiagram;
import model.CollectionType;
import model.Type;
import model.Visibility;

public class AttributeGenerator extends Generator {

	public AttributeGenerator() {
		super();

	}

	public void handle(ClassDiagram cd) {
		generateMandatory(cd);
		generateFromAssociations(cd);

		super.handle(cd);
	}

	private void generateFromAssociations(ClassDiagram cd) {
		for (Association ass : cd.getAssociations().values()) {
			Class c1 = ass.getClass1();
			Class c2 = ass.getClass2();

			Type c1Type = Type.getType(c1);
			Type c2Type = Type.getType(c2);

			String c1Name = (ass.getRollName2().equals(c2.getName()) ?  StringUtils.decapitalize(ass.getRollName2()) :  ass.getRollName2() + "_" + c2.getName());
			String c2Name = (ass.getRollName1().equals(c1.getName()) ? StringUtils.decapitalize(ass.getRollName1()) :   ass.getRollName1() + "_" + c1.getName());

			if (ass.getMultiplicity1().contains("*"))
				c1Type = CollectionType.getList(c1.getName());
			if (ass.getMultiplicity2().contains("*"))
				c2Type = CollectionType.getList(c2.getName());

			c1.addAttribute(new Attribute(c1Name, c2Type, Visibility.PRIVATE));
			c2.addAttribute(new Attribute(c2Name, c1Type, Visibility.PRIVATE));
		}
	}

	private void generateMandatory(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			// add collection for all created objects
			clas.addAttribute(new Attribute("XUML_INSTANCES", CollectionType.getSet(clas.getName()), Visibility.PRIVATE, true, true, "new HashSet<>()"));

			clas.addAttribute(new Attribute("XUML_stateMachine", Type.getStateMachine(), Visibility.PRIVATE));
		}
	}
}
