package generation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Association;
import model.AssociationClass;
import model.Attribute;
import model.Class;
import model.ClassDiagram;
import model.Method;
import model.Type;
import model.Visibility;
import model.state.Event;
import model.state.State;
import model.state.StateMachine;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

@SuppressWarnings("unchecked")
public class XmlParser {
	private File file;

	public XmlParser(String file) {
		this.file = new File(file);
	}

	public ClassDiagram parse() {
		SAXBuilder builder = new SAXBuilder();

		try {
			Document document = (Document) builder.build(this.file);
			Element rootNode = document.getRootElement();
			Element classesNode = rootNode.getChild("classes");
			Element associationsNode = rootNode.getChild("associations");
			Element associationClassesNode = rootNode.getChild("association-classes");

			Map<String, Class> classes = parseClasses(classesNode);
			Map<String, AssociationClass> associationsClasses = parseAssociationClasses(associationClassesNode);
			Map<String, Class> allClasses = new HashMap<>();
			allClasses.putAll(classes);
			allClasses.putAll(associationsClasses);

			Map<String, Association> associations = parseAssociations(associationsNode, allClasses);

			linkAssociationClassesToAssociations(associations, associationsClasses, associationClassesNode);

			return new ClassDiagram(classes, associations, associationsClasses);
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}

		// TODO weird?
		return null;
	}

	private void linkAssociationClassesToAssociations(Map<String, Association> associations, Map<String, AssociationClass> associationsClasses,
			Element associationClassesNode) {
		if (associationClassesNode == null)
			return;

		for (Element associationClassElement : (List<Element>) associationClassesNode.getChildren()) {
			String name = associationClassElement.getChildText("name");
			String association = associationClassElement.getChildText("association");

			associationsClasses.get(name).setAssociation(associations.get(association));
		}
	}

	private Map<String, AssociationClass> parseAssociationClasses(Element associationClassesNode) {
		Map<String, AssociationClass> associationClasses = new HashMap<>();

		if (associationClassesNode == null)
			return associationClasses;

		// parse classes but ommit associations for now
		for (Element associationClassElement : (List<Element>) associationClassesNode.getChildren()) {
			String name = associationClassElement.getChildText("name");
			Map<String, Attribute> attributes = parseAttributes(associationClassElement.getChild("attributes"));

			
			Set<String> implement = parseImplements(associationClassElement.getChild("implements"));
			StateMachine stateMachine = parseStateMachine(associationClassElement.getChild("state-machine"));
			Map<String, Method> methods = parseMethods(associationClassElement.getChild("methods"));

			AssociationClass associationClass = new AssociationClass(Visibility.PUBLIC, name, "", implement, attributes, stateMachine, methods);
			associationClasses.put(name, associationClass);
		}

		return associationClasses;
	}

	// private Map<String, AssociationClass> parseAssociationClasses(Element
	// associationClassesNode, Map<String, Association> associations) {
	// Map<String, AssociationClass> associationClasses = new HashMap<>();
	//
	// if (associationClassesNode == null)
	// return associationClasses;
	//
	// for (Element associationClassElement : (List<Element>)
	// associationClassesNode.getChildren()) {
	// String name = associationClassElement.getChildText("name");
	// Map<String, Attribute> attributes =
	// parseAttributes(associationClassElement.getChild("attributes"));
	// String association = associationClassElement.getChildText("association");
	//
	// Set<String> implement =
	// parseImplements(associationClassesNode.getChild("implements"));
	// StateMachine stateMachine =
	// parseStateMachine(associationClassesNode.getChild("state-machine"));
	// Map<String,Method> methods =
	// parseMethods(associationClassesNode.getChild("methods"));
	//
	// AssociationClass associationClass = new
	// AssociationClass(Visibility.PUBLIC, name, "", implement, attributes,
	// stateMachine,
	// associations.get(association), methods);
	// associationClasses.put(name, associationClass);
	// }
	//
	// return associationClasses;
	// }

	private Map<String, Association> parseAssociations(Element node, Map<String, Class> classes) {
		Map<String, Association> associations = new HashMap<>();

		if (node == null)
			return associations;

		for (Element classAss : (List<Element>) node.getChildren()) {
			String name = classAss.getChildText("name");

			Element associationEnd1 = classAss.getChild("association-end-1");
			String class1 = associationEnd1.getChildText("class");
			String multiplicity1 = associationEnd1.getChildText("multiplicity");
			String roleName1 = associationEnd1.getChildText("role-name");

			Element associationEnd2 = classAss.getChild("association-end-2");
			String class2 = associationEnd2.getChildText("class");
			String multiplicity2 = associationEnd2.getChildText("multiplicity");
			String roleName2 = associationEnd2.getChildText("role-name");

			associations.put(name, new Association(name, classes.get(class1), classes.get(class2), multiplicity1, multiplicity2, roleName1, roleName2));
		}

		return associations;
	}

	private Map<String, Class> parseClasses(Element node) {
		Map<String, Class> classes = new HashMap<>();

		if (node == null)
			return classes;

		for (Element clasEl : (List<Element>) node.getChildren()) {
			Class clas = parseClass(clasEl);
			classes.put(clas.getName(), clas);
		}

		return classes;
	}

	private Class parseClass(Element node) {
		String name = node.getChildText("name");
		String extend = node.getChildText("extends");
		if (extend == null)
			extend = "";

		Element implNode = node.getChild("implements");
		Element attrNode = node.getChild("attributes");
		Element smNode = node.getChild("state-machine");
		Element methodsNode = node.getChild("methods");

		Set<String> impl = parseImplements(implNode);
		Map<String, Attribute> attr = parseAttributes(attrNode);
		StateMachine sm = parseStateMachine(smNode);
		Map<String, Method> methods = parseMethods(methodsNode);

		// TODO add visiblity to xml fileF
		return new Class(Visibility.PUBLIC, name, extend, impl, attr, sm, methods);
	}

	private Map<String, Method> parseMethods(Element methodsNode) {
		Map<String, Method> methods = new HashMap<String, Method>();

		if (methodsNode == null)
			return methods;

		for (Element methodEl : (List<Element>) methodsNode.getChildren()) {
			Method method = parseMethod(methodEl);
			method.setUmlSpecified(true);
			methods.put(method.getName(), method);
		}

		return methods;
	}

	// <method>
	// <name>getBalance</name>
	// <type>int</type>
	// <visibility>public</visibility>
	// <parameters></parameters>
	// <implementation></implementation>
	// </method>

	private Method parseMethod(Element methodEl) {
		String name = methodEl.getChildText("name");
		System.out.println(name);
		Type type = Type.getType(methodEl.getChildText("type"));
		Visibility visibility = parseVisibility(methodEl.getChildText("visibility"));
		Map<String, Attribute> attributes = parseAttributes(methodEl.getChild("parameters"));

		String implementation = methodEl.getChildText("implementation");

		return new Method(name, visibility, type, attributes, implementation);
	}

	private StateMachine parseStateMachine(Element node) {
		Map<String, State> states = new HashMap<>();
		Map<String, Event> events = new HashMap<>();

		if (node == null)
			return new StateMachine(states, events, "");

		String defaultState = node.getAttributeValue("defaultState");

		for (Element stateNode : (List<Element>) node.getChild("states").getChildren()) {
			int number = Integer.parseInt(stateNode.getChildText("number"));
			String name = stateNode.getChildText("name").trim();
			String procedure = stateNode.getChildText("procedure");

			Map<String, Attribute> parameters = new HashMap<>();

			if (stateNode.getChild("parameters") != null) {
				for (Element e : (List<Element>) stateNode.getChild("parameters").getChildren()) {
					String paramName = e.getChild("name").getText();
					Type paramType = convertToJavaType(e.getChild("type").getText());

					Attribute attribute = new Attribute(paramName, paramType);

					parameters.put(paramName, attribute);
				}
			}

			states.put(name, new State(name, number, procedure, parameters));
		}

		for (Element stateNode : (List<Element>) node.getChild("events").getChildren()) {
			String from = stateNode.getChildText("from");
			String to = stateNode.getChildText("to").trim();
			String name = stateNode.getChildText("name");

			Event event = new Event(name, states.get(from), states.get(to));
			events.put(name, event);
		}

		return new StateMachine(states, events, defaultState);
	}

	private Set<String> parseImplements(Element node) {
		Set<String> impl = new HashSet<>();
		if (node != null)
			for (Element implNode : (List<Element>) node.getChildren()) {
				impl.add(implNode.getText());
			}

		return impl;
	}

	private Map<String, Attribute> parseAttributes(Element node) {
		Map<String, Attribute> attr = new HashMap<>();

		if (node == null)
			return attr;

		for (Element attrNode : (List<Element>) node.getChildren()) {
			String name = attrNode.getChild("name").getText();

			Type type = null;

			type = convertToJavaType(attrNode.getChild("type").getText());

			Visibility visibility = null;

			visibility = parseVisibility(attrNode.getChildText("visibility"));

			attr.put(name, new Attribute(name, type, visibility));
		}
		return attr;
	}

	private Type convertToJavaType(String type) {
		switch (type) {
		case "int":
			return  Type.getInt();
		case "bool":
			return Type.getBool();
		case "string":
			return Type.getString();
		case "date":
			return Type.getDate();
		default:
			return Type.getType(type);
		}
	}

	private Visibility parseVisibility(String visibility) {

		switch (visibility) {
		case "private":
			return Visibility.PRIVATE;
		case "public":
			return Visibility.PUBLIC;
		case "protected":
			return Visibility.PROTECTED;
		case "":
			return Visibility.NONE;
		default:
			throw new RuntimeException("this visibility parameter is not supported");
		}
	}
}
