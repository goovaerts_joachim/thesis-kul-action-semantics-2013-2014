package generation;

import model.Association;
import model.AssociationClass;
import model.ClassDiagram;
import model.Class;

public class AssociationClassGenerator extends Generator {

	@Override
	public void handle(ClassDiagram classDiagram) {
		for (AssociationClass associationClass : classDiagram.getAssociationClasses().values()) {
			Association association = associationClass.getAssociation();
			classDiagram.getAssociations().remove(association.getName());
			
			Class class1 = association.getClass1();
			Class class2 = association.getClass2();

			Association class1ToAssociation = new Association(class1.getName() + "To" + associationClass.getName(), class1, associationClass, "1",
					association.getMultiplicity2(),  association.getRollName1(), associationClass.getName());
			Association class2ToAssociation = new Association(class2.getName() + "To" + associationClass.getName(), class2, associationClass,
					 "1", association.getMultiplicity1(),association.getRollName2(), associationClass.getName());

			classDiagram.getClasses().put(associationClass.getName(), associationClass);
			classDiagram.getAssociations().put(class1ToAssociation.getName(), class1ToAssociation);
			classDiagram.getAssociations().put(class2ToAssociation.getName(), class2ToAssociation);
		}

		super.handle(classDiagram);
	}

}
