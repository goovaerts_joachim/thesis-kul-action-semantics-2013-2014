package generation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Association;
import model.Attribute;
import model.ClassDiagram;
import model.Method;
import model.Type;

public class ImportGenerator extends Generator {
	private Map<String, String> importMappings;

	public ImportGenerator() {
		super();

		importMappings = new HashMap<>();

		importMappings.put("HashMap", "java.util.HashMap");
		importMappings.put("HashSet", "java.util.HashSet");
		importMappings.put("Map", "java.util.Map");
		importMappings.put("Set", "java.util.Set");
		importMappings.put("XUML_Event", "xuml.XUML_Event");
		importMappings.put("XUML_EventInstance", "xuml.XUML_EventInstance");
		importMappings.put("XUML_SignalReceiver", "xuml.XUML_SignalReceiver");
		importMappings.put("XUML_State", "xuml.XUML_State");
		importMappings.put("XUML_StateMachine", "xuml.XUML_StateMachine");
	}

	public void handle(ClassDiagram cd) {
//		for (model.Class clas : cd.getClasses().values()) {
//			for (Association ass : clas.getAssociations())
//				clas.addImport(ass.getOtherClass(clas).getName());
//
//			for (Attribute attr : clas.getAttributes().values()) {
//				addImport(clas, attr.getType());
//			}
//
//			for (Method method : clas.getMethods().values()) {
//				addImport(clas, method.getReturnType());
//			}
//		}
//
		super.handle(cd);
	}

	private void addImport(model.Class clas, Type type) {
		if (this.importMappings.containsKey(type.getType()))
			clas.addImport(this.importMappings.get(type.getType()));
		else
			clas.addImport(type.getType());
	}
}
