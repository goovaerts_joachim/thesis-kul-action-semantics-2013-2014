package generation;

import model.ClassDiagram;

public abstract class Generator {
	protected Generator next;

	public void setNext(Generator next) {
		this.next = next;
	}

	public void handle(ClassDiagram classDiagram) {
		if (next != null)
			next.handle(classDiagram);

	}
}
