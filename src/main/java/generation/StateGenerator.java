package generation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Attribute;
import model.Class;
import model.ClassDiagram;
import model.CollectionType;
import model.Method;
import model.Type;
import model.Visibility;
import model.state.Event;
import model.state.State;
import model.state.StateMachine;
import utils.StringUtils;

public class StateGenerator extends Generator {

	public StateGenerator() {

	}

	public void handle(ClassDiagram cd) {
		generateInnerClasses(cd);
		generateStateInitiateMethod(cd);
		generateProcessMethod(cd);
		generateIsValidTransitionMethod(cd);

		super.handle(cd);
	}

	// generates an innerclass for each state. the process function should be
	// executed on state change
	private void generateInnerClasses(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			if (clas.isXumlClass())
				continue;

			StateMachine sm = clas.getStateMachine();

			for (State state : sm.getStates().values()) {
				Class innerClass = new Class(Visibility.PRIVATE, StringUtils.toCamelCase(state.getName()), "",new HashSet<String>() , new HashMap<String, Attribute>());

				String procedure = "";

				for (String paramName : state.getParameters().keySet()) {
					Attribute attribute = state.getParameters().get(paramName);
					procedure += attribute.getType() + " " + paramName + "=" + "(" + attribute.getType() + ")" + "parameters.get(\"" + paramName + "\");\n";
				}

				procedure += state.getJavaProcedure();

				HashMap<String, Attribute> attributes = new HashMap<>();
				attributes.put("parameters", new Attribute("parameters", CollectionType.getMap("String", "Object")));

				Method stateAction = new Method("process", Visibility.PUBLIC, Type.getVoid(), attributes, procedure);

				innerClass.addMethod(stateAction);

				clas.addInnerClass(innerClass);
			}
		}
	}

	// this generated method instantiates the statemachine in each class
	private void generateStateInitiateMethod(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			if (clas.isXumlClass())
				continue;

			String procedure = "";
			String defaultState = "";

			for (Class inner : clas.getInnerClasses().values()) {
				if (clas.getStateMachine().getDefaultState().getName().equalsIgnoreCase(inner.getName())) {
					defaultState = inner.getName().toLowerCase();
				}

				procedure += inner.getName() + " " + inner.getName().toLowerCase() + " = new " + inner.getName() + "();\n";
				procedure += "XUML_stateMachine.addState(" + inner.getName().toLowerCase() + ");\n\n";
			}

			procedure += generatePossibleTransitions(clas);
			procedure += defaultState + ".process(new HashMap<String,Object>());"; // set
																					// to
																					// initial
																					// state
			procedure += "XUML_stateMachine.setInitialState(" + defaultState + ");";

			clas.addMethod(new Method("XUML_initiateStateMachine", Visibility.PRIVATE, Type.getVoid(), new HashMap<String, Attribute>(), procedure));
		}
	}

	// this method generates all possible state transitions
	private String generatePossibleTransitions(Class clas) {
		String trans = "";
		int uniqueIdForEvent = 0;

		Map<String, Integer> eventNumbers = new HashMap<>();
		Set<String> alreadyAddedAttributes = new HashSet<>();

		for (State state : clas.getStateMachine().getStates().values()) {
			for (Event event : state.getOutgoingEvents()) {
				if (!eventNumbers.containsKey(event.getName()))
					eventNumbers.put(event.getName(), uniqueIdForEvent++);

				String fromState = event.getFrom().getName().toLowerCase().replaceAll(" ", "");
				String toState = event.getTo().getName().toLowerCase().replaceAll(" ", "");

				trans += "XUML_stateMachine.addEvent(new XUML_Event(\"" + event.getName() + "\"," + fromState + "," + toState + ","
						+ eventNumbers.get(event.getName()) + "));\n";

				// add static field with unique id for each event
				if (!alreadyAddedAttributes.contains(event.getName())) {
					Attribute attr = new Attribute(event.getName().toUpperCase(), Type.getInt(), Visibility.PUBLIC, true, true, ""
							+ eventNumbers.get(event.getName()));
					clas.addAttribute(attr);
					alreadyAddedAttributes.add(event.getName());
				}
			}
		}

		return trans;
	}

	// method for each class to handel incomming signals
	private void generateProcessMethod(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			if (clas.isXumlClass())
				continue;

			HashMap<String, Attribute> attributes = new HashMap<>();
			attributes.put("eventInstance", new Attribute("eventInstance", Type.getEventInstance()));

			String procedure = "if(!isValidTransition(eventInstance))\n"
					+ "return; \n"
					+ "XUML_State from = XUML_stateMachine.getEvents().get(eventInstance.getEventNumber()+ XUML_stateMachine.getCurrentState().toString()).getFrom();\n"
					+ "	XUML_State to = XUML_stateMachine.getEvents().get(eventInstance.getEventNumber()+ XUML_stateMachine.getCurrentState().toString()).getTo();\n"
					+ "	\n"
					+ "	if((this.XUML_stateMachine.getCurrentState().equals(from) || this.XUML_stateMachine.getCurrentState() == null) && this.XUML_stateMachine.getStates().contains(to)) {\n"
					+ "		XUML_stateMachine.setCurrentState(to);" + "to.process(eventInstance.getParameters());\n" + "	}";

			Method method = new Method("XUML_process", Visibility.PUBLIC, Type.getVoid(), attributes, procedure);
			clas.addMethod(method);
		}
	}

	private void generateIsValidTransitionMethod(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			if (clas.isXumlClass())
				continue;

			HashMap<String, Attribute> attributes = new HashMap<>();
			attributes.put("eventInstance", new Attribute("eventInstance", Type.getEventInstance()));
			String procedure = String
					.format("return XUML_stateMachine.getEvents().get(eventInstance.getEventNumber() + XUML_stateMachine.getCurrentState().toString()) != null;");

			Method method = new Method("isValidTransition", Visibility.PRIVATE, Type.getBool(), attributes, procedure);

			clas.addMethod(method);
		}

	}

	// TODO move to appropriate place


}
