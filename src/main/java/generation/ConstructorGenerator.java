package generation;

import java.util.HashSet;

import model.Attribute;
import model.Class;
import model.ClassDiagram;
import model.Constructor;
import model.Visibility;

public class ConstructorGenerator extends Generator {

	public ConstructorGenerator() {
		super();
	}

	public void handle(ClassDiagram cd) {
		generateDefaultConstructors(cd);

		super.handle(cd);
	}

	private void generateDefaultConstructors(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			Constructor constructor = new Constructor(Visibility.PUBLIC, clas.getName(), new HashSet<Attribute>(), getMandatoryConstructorProcedure());
			clas.addConstructor(constructor);
		}
	}

	private String getMandatoryConstructorProcedure() {
		return "XUML_stateMachine = new XUML_StateMachine();\n" + "XUML_initiateStateMachine();";
	}
}
