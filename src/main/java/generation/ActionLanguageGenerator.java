package generation;
import model.ClassDiagram;
import model.Class;
import model.Method;
import model.state.State;

public class ActionLanguageGenerator extends Generator {

	@Override
	public void handle(ClassDiagram classDiagram) {
		//translateUmlSpecifiedMethods(classDiagram);
		translateStateActionLanguage(classDiagram);
		
		super.handle(classDiagram);
	}

	private void translateStateActionLanguage(ClassDiagram classDiagram) {
		int teller = 0;

		for(Class clas:classDiagram.getClasses().values()) {
			if(clas.getStateMachine() == null)
				continue;
			System.out.println(clas.getName());
			for(State state: clas.getStateMachine().getStates().values()) {
				System.out.println(state.getName());
				System.out.println(state.getActionLanguageProcedure());
				System.out.println("---------------------");
				
				try {
				String javaProcedure = ActionLanguageParser.parseActionLanguage(state.getActionLanguageProcedure(),state.getParameters(), clas,classDiagram);
				
				System.out.println(javaProcedure);
				System.out.println("######################");
				state.setJavaProcedure(javaProcedure);
				}
				catch (Exception e){
					e.printStackTrace();
					teller++;
				}
				
			}
		}
		System.out.println("fouten: " + teller);
	}
	
	private void translateUmlSpecifiedMethods(ClassDiagram classDiagram) {
		for(String className:classDiagram.getClasses().keySet()) {
			Class clas = classDiagram.getClasses().get(className);
			for(Method m: clas.getMethods().values()) {
				if(m.isUmlSpecified()){
					String javaProcedure = ActionLanguageParser.parseActionLanguage(m.getCode(),m.getAttributes(), clas, classDiagram);
					m.setCode(javaProcedure);
				}
			}
		}
	}
}
