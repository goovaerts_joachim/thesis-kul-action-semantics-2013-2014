package generation;

import model.Class;
import model.ClassDiagram;

public class ImplementsGenerator extends Generator {
	public ImplementsGenerator() {
		super();
	}

	public void handle(ClassDiagram cd) {
		for (Class clas : cd.getClasses().values()) {
			clas.addImplement("XUML_SignalReceiver");

			for (Class innerClass : clas.getInnerClasses().values())
				innerClass.addImplement("XUML_State");
		}

		super.handle(cd);
	}
}
