package generation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import model.Attribute;
import model.Class;
import model.ClassDiagram;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;

import frontend.ASLexer;
import frontend.ASParser;
import translation.JavaVisitor;

public class ActionLanguageParser {
	
	public static String parseActionLanguage(String actionLanguage,Map<String,Attribute> procedureParams, Class clas, ClassDiagram cd) {
		// Reading the DSL script
		
		InputStream is = new ByteArrayInputStream(actionLanguage.getBytes());
		CharStream cs;
		try {
			cs = new ANTLRInputStream(is);
			ASLexer lexer = new ASLexer(cs);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			ASParser parser = new ASParser(tokens);
			parser.setBuildParseTree(true);
			ASParser.ProcContext tree = parser.proc();
			JavaVisitor java = new JavaVisitor(procedureParams, clas, cd);
			
			return java.visit(tree).getSuffix(true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("ActionLanguageParser");
		} 
		
//		InputStream is = new ByteArrayInputStream(actionLanguage.getBytes());
//
//				// Loading the DSL script into the ANTLR stream.
//				CharStream cs;
//				try {
//					cs = new ANTLRInputStream(is);
//				} catch (IOException e) {
//					return "ERROR: " + e.getMessage();
//				}
//
//				// Passing the input to the lexer to create tokens
//				ASLexer lexer = new ASLexer(cs);
//
//				CommonTokenStream tokens = new CommonTokenStream(lexer);
//
//				// Passing the tokens to the parser to create the parse trea.
//				ASParser parser = new ASParser(tokens);
//				parser.setBuildParseTree(true);

				// Adding the listener to facilitate walking through parse tree.
				//parser.addParseListener(new MyASBaseListener());

				// invoking the parser.
//				ASParser.ProcContext tree = parser.proc();
				//System.out.println(tree.toStringTree(parser));
				
				
	}
}
