package generation;

import java.util.HashMap;
import java.util.Map;

import model.Association;
import model.AssociationClass;
import model.Attribute;
import model.Class;
import model.ClassDiagram;
import model.CollectionType;
import model.Method;
import model.Type;
import model.Visibility;
import utils.StringUtils;

public class MethodGenerator extends Generator {

	@Override
	public void handle(ClassDiagram classDiagram) {
		generateLinkMethods(classDiagram);
		generateUnLinkMethods(classDiagram);

		generateCreateActionLanguage(classDiagram);
		generateSelectAllLanguage(classDiagram);
		// generateFilterActionLanguage(classDiagram);
		generateStaticMethodsForLinks(classDiagram);

		generateSignalMethods(classDiagram);

		generateGettersForEachAttribute(classDiagram);
		generateGettersForPassingAssociationClass(classDiagram);

		super.handle(classDiagram);
	}

	private void generateLinkMethods(ClassDiagram classDiagram) {
		for (Association ass : classDiagram.getAssociations().values()) {

			Class class1 = ass.getClass1();
			Class class2 = ass.getClass2();

			String class1Attribute = "";
			String class2Attribute = "";

			if (classDiagram.getAssociationClasses().containsKey(
					class1.getName()))
				class1Attribute = StringUtils.decapitalize(ass.getRollName1());
			else
				class1Attribute = ass.getRollName1() + "_" + class1.getName();
			
			if (classDiagram.getAssociationClasses().containsKey(
					class2.getName()))
				class2Attribute = StringUtils.decapitalize(ass.getRollName2());
			else
				class2Attribute = ass.getRollName2() + "_" + class2.getName();
			

			Method methodClass2 = generateLinkMethod(ass, class1, class2,
					class1Attribute, ass.getRollName1());
			class2.addMethod(methodClass2);

			Method methodClass1 = generateLinkMethod(ass, class2, class1,
					class2Attribute, ass.getRollName2());
			class1.addMethod(methodClass1);
		}
	}

	private Method generateLinkMethod(Association ass, Class class1,
			Class class2, String class1Attribute, String rollName) {
		Map<String, Attribute> attributes = new HashMap<String, Attribute>();
		Attribute attribute = new Attribute(class1.getName().toLowerCase(),
				Type.getType(class1));
		attributes.put(attribute.getName(), attribute);

		String procedure = "";

		if (ass.getCorrespondingMultiplicity(rollName).equals("0..1")
				|| ass.getCorrespondingMultiplicity(rollName).equals("1")) {
			procedure += "if(this." + class1Attribute + "!="
					+ attribute.getName() + "){\n";
			procedure += "this." + class1Attribute + " = "
					+ attribute.getName() + ";\n";
		} else {
			procedure += "if(!this." + class1Attribute + ".contains("
					+ attribute.getName() + ")){\n";
			procedure += "this." + class1Attribute + ".add("
					+ attribute.getName() + ");\n";
		}

		procedure += attribute.getName() + "." + ass.getOtherRollName(rollName)
				+ "_" + class2.getName() + "(this);\n}";

		return new Method(class1Attribute, Visibility.PUBLIC, Type.getVoid(),
				attributes, procedure);

	}

	private void generateUnLinkMethods(ClassDiagram classDiagram) {
		for (Association ass : classDiagram.getAssociations().values()) {
			Class class1 = ass.getClass1();
			Class class2 = ass.getClass2();

			Method methodClass2 = generateUnlinkMethod(ass, class1,
					ass.getRollName1());
			class2.addMethod(methodClass2);

			Method methodClass1 = generateUnlinkMethod(ass, class2,
					ass.getRollName2());
			class1.addMethod(methodClass1);
		}
	}

	private Method generateUnlinkMethod(Association ass, Class class1,
			String rollname) {
		Map<String, Attribute> attributes = new HashMap<String, Attribute>();
		Attribute attribute = new Attribute(class1.getName().toLowerCase(),
				Type.getType(class1));
		attributes.put(attribute.getName(), attribute);

		String procedure = "";
		if (ass.getMultiplicity1().equals("0..1")
				|| ass.getMultiplicity1().equals("1"))
			procedure = "this." + rollname + "_" + class1.getName()
					+ " = null;";
		else
			procedure = "this." + rollname + "_" + class1.getName()
					+ ".remove(" + attribute.getName() + ");";

		return new Method("unLink" + StringUtils.capitalize(rollname) + "_"
				+ class1.getName(), Visibility.PUBLIC, Type.getVoid(),
				attributes, procedure);
	}

	private void generateCreateActionLanguage(ClassDiagram classDiagram) {
		for (Class clas : classDiagram.getClasses().values()) {
			// add create method
			String procedure = "";
			procedure += String.format("%s %s = new %s();\n", clas.getName(),
					clas.getName().toLowerCase(), clas.getName());
			procedure += String.format("XUML_INSTANCES.add(%s);\n", clas
					.getName().toLowerCase());
			procedure += String.format("return %s;", clas.getName()
					.toLowerCase());

			Method method = new Method("create", Visibility.PUBLIC,
					Type.getType(clas), new HashMap<String, Attribute>(),
					procedure);
			method.setIsStatic(true);
			clas.addMethod(method);
		}
	}

	private void generateSelectAllLanguage(ClassDiagram classDiagram) {
		for (Class clas : classDiagram.getClasses().values()) {
			String procedure = "";
			procedure += String.format("return XUML_INSTANCES;");

			Method method = new Method("all" + clas.getName() + "Objects",
					Visibility.PUBLIC, CollectionType.getSet(clas.getName()),
					new HashMap<String, Attribute>(), procedure);
			method.setIsStatic(true);
			clas.addMethod(method);
		}
	}

	private void generateSignalMethods(ClassDiagram classDiagram) {
		for (Class clas : classDiagram.getClasses().values()) {
			if (clas.isXumlClass())
				continue;

			clas.addMethod(generateSignalMethod("signal", false));
			clas.addMethod(generateSignalMethod("delayedSignal", true));
		}
	}

	private Method generateSignalMethod(String methodName, boolean delayedSignal) {
		String procedure = "";

		Map<String, Attribute> attributes = new HashMap<String, Attribute>();
		attributes.put("event", new Attribute("event", Type.getInt()));
		String delay = "0";
		if (delayedSignal) {
			attributes.put("delay", new Attribute("delay", Type.getInt()));
			delay = "delay";
		}

		procedure += String
				.format("return new XUML_EventInstance(event, this, " + delay
						+ ");");

		return new Method(methodName, Visibility.PUBLIC,
				Type.getEventInstance(), attributes, procedure);
	}

	private void generateGettersForEachAttribute(ClassDiagram classDiagram) {
		for (Class clas : classDiagram.getClasses().values()) {
			for (Attribute attr : clas.getAttributes().values()) {
				if (attr.isStatic())
					continue;

				String procedure = "";
				procedure += String.format("return this.%s;", attr.getName());
				String methodName = "";
				methodName += String.format("get%s",
						StringUtils.capitalize(attr.getName()));
				Method method = new Method(methodName, Visibility.PUBLIC,
						attr.getType(), new HashMap<String, Attribute>(),
						procedure);
				clas.addMethod(method);
			}
		}
	}

	// TODO only works with lists, improve that it works with other collections
	private void generateGettersForPassingAssociationClass(
			ClassDiagram classDiagram) {
		for (AssociationClass assClass : classDiagram.getAssociationClasses()
				.values()) {
			Association association = assClass.getAssociation();
			Class class1 = association.getClass1();
			Class class2 = association.getClass2();

			String returnVarName = "all" + class2.getName();

			String procedure = CollectionType.getList(class2.getName())
					.getType() + returnVarName + "= new ArrayList<>();\n";
			procedure += "for(" + assClass.getName() + " "
					+ assClass.getName().toLowerCase() + ":"
					+ assClass.getName() + "_" + assClass.getName() + "){\n";
			procedure += returnVarName + ".add("
					+ assClass.getName().toLowerCase() + ".get"
					+ StringUtils.capitalize(association.getRollName1()) + "_"
					+ class2.getName() + "());\n";
			procedure += "}\n";
			procedure += "return " + returnVarName + ";";

			Method method = new Method("getAll" + class2.getName(),
					Visibility.PUBLIC,
					CollectionType.getList(class2.getName()),
					new HashMap<String, Attribute>(), procedure);
			class1.addMethod(method);
		}
	}

	private void generateStaticMethodsForLinks(ClassDiagram classDiagram) {
		// generateLinkMethodsForAssociationClass(classDiagram);
		// generateLinkedToMethodsForAssociationClass(classDiagram);
	}

	// private void generateLinkedToMethodsForAssociationClass(ClassDiagram
	// classDiagram) {
	// for (Association ass : classDiagram.getAssociations()) {
	// List<Attribute> attr = new ArrayList<>();
	// Attribute attribute = new
	// Attribute(ass.getClass1().getName().toLowerCase(),
	// Type.getType(ass.getClass1()));
	// attr.add(attribute);
	// String procedure = "";
	// procedure += String.format("return %s.get%s();", attribute.getName(),
	// capitalize(ass.getClass2().getName().toLowerCase()));
	//
	// Method linkedTo = new Method("linkedTo", Visibility.PUBLIC,
	// Type.getType(ass.getClass2()), attr, procedure);
	// linkedTo.setIsStatic(true);
	// classDiagram.getClasses().get(ass.getName()).addMethod(linkedTo);
	//
	// attr = new ArrayList<>();
	// attribute = new Attribute(ass.getClass2().getName().toLowerCase(),
	// Type.getType(ass.getClass2()));
	// attr.add(attribute);
	// procedure = "";
	// procedure += String.format("return %s.get%s();", attribute.getName(),
	// capitalize(ass.getClass1().getName().toLowerCase()));
	//
	// linkedTo = new Method("linkedTo", Visibility.PUBLIC,
	// Type.getType(ass.getClass1()), attr, procedure);
	// linkedTo.setIsStatic(true);
	// classDiagram.getClasses().get(ass.getName()).addMethod(linkedTo);
	// }
	// }

	// private void generateLinkMethodsForAssociationClass(ClassDiagram
	// classDiagram) {
	// for (Association ass : classDiagram.getAssociations()) {
	//
	// Class clas = new Class(Visibility.PUBLIC, ass.getName(), "", new
	// HashSet<String>(), new ArrayList<Attribute>());
	// // indicates that this class has no statemachine
	// clas.setXumlClass(true);
	//
	// List<Attribute> linkAttr = new ArrayList<>();
	// Attribute linkAtt1 = new
	// Attribute(ass.getClass1().getName().toLowerCase(),
	// Type.getType(ass.getClass1()));
	// Attribute linkAtt2 = new
	// Attribute(ass.getClass2().getName().toLowerCase(),
	// Type.getType(ass.getClass2()));
	// linkAttr.add(linkAtt1);
	// linkAttr.add(linkAtt2);
	//
	// String procedure = "";
	// procedure += String.format("%s.linkTo%s(%s);\n", linkAtt1.getName(),
	// ass.getClass2().getName(), linkAtt2.getName());
	// procedure += String.format("%s.linkTo%s(%s);", linkAtt2.getName(),
	// ass.getClass1().getName(), linkAtt1.getName());
	//
	// Method link = new Method("link", Visibility.PUBLIC, Type.getVoid(),
	// linkAttr, procedure);
	// link.setIsStatic(true);
	// clas.addMethod(link);
	//
	// List<Attribute> linkAttr2 = new ArrayList<>();
	// Attribute linkAtt21 = new
	// Attribute(ass.getClass1().getName().toLowerCase(),
	// Type.getType(ass.getClass1()));
	// Attribute linkAtt22 = new
	// Attribute(ass.getClass2().getName().toLowerCase(),
	// Type.getType(ass.getClass2()));
	// linkAttr2.add(linkAtt22);
	// linkAttr2.add(linkAtt21);
	//
	// String procedure2 = "";
	// procedure2 += String.format("%s.linkTo%s(%s);", linkAtt21.getName(),
	// ass.getClass2().getName(), linkAtt22.getName());
	// procedure2 += String.format("%s.linkTo%s(%s);", linkAtt22.getName(),
	// ass.getClass1().getName(), linkAtt21.getName());
	//
	// Method link2 = new Method("link", Visibility.PUBLIC, Type.getVoid(),
	// linkAttr2, procedure2);
	// link.setIsStatic(true);
	// clas.addMethod(link2);
	// classDiagram.getClasses().put(clas.getName(), clas);
	//
	// }
	// }
}
